package com.example.groupmanagerdam;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class configurarUsuario extends BaseMenu {
    public TextView etEditarCuentaPass, etEditarCuentaNombre, etEditarCuentaCorreo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configurar_usuario);

        etEditarCuentaPass  = findViewById(R.id.etEditarCuentaPass);
        etEditarCuentaNombre  = findViewById(R.id.etEditarCuentaNombre);
        etEditarCuentaCorreo  = findViewById(R.id.etEditarCuentaCorreo);

        iniciarSesion();
        activarPermisos();
        inicializarFirebase();

        Query query = dr.child("Usuario").child(usuSesion.getUid_usuario());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists())
                {
                    String pass = (String) snapshot.child("contrasena").getValue();
                    String nombre = (String) snapshot.child("nombre").getValue();
                    String correo = (String) snapshot.child("correo").getValue();
                    etEditarCuentaPass.setText(pass);
                    etEditarCuentaNombre.setText(nombre);
                    etEditarCuentaCorreo.setText(correo);
                }
                else
                {
                    Toast.makeText(getBaseContext(),"No se ha encontrado informacion", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getBaseContext(),"Error al consultar", Toast.LENGTH_SHORT).show();
            }
        });


    }

    public void onEditar(View view)
    {
        String contrasena = etEditarCuentaPass.getText().toString();
        String correo = etEditarCuentaCorreo.getText().toString();
        String nombre = etEditarCuentaNombre.getText().toString();

        if(!TextUtils.isEmpty(contrasena) && !TextUtils.isEmpty(correo) && !TextUtils.isEmpty(nombre))
        {
            Query query = dr.child("Usuario").child(usuSesion.getUid_usuario());
            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if(snapshot.exists())
                    {
                        dr.child("Usuario").child(usuSesion.getUid_usuario()).child("contrasena").setValue(contrasena);
                        dr.child("Usuario").child(usuSesion.getUid_usuario()).child("nombre").setValue(nombre);
                        dr.child("Usuario").child(usuSesion.getUid_usuario()).child("correo").setValue(correo);

                        Toast.makeText(getBaseContext(),"Se ha editado el usuario", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(getBaseContext(),"No se ha encontrado informacion", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Toast.makeText(getBaseContext(),"Error al consultar", Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
        {
            Toast.makeText(this,"Rellene los campos", Toast.LENGTH_SHORT).show();
        }



    }

}
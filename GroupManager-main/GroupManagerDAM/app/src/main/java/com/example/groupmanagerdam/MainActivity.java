package com.example.groupmanagerdam;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.groupmanagerdam.models.usuario;
import com.example.groupmanagerdam.utils.sesion;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends BaseFirebase {

    private TextView etLoginUsuario, etLoginPassword;
    private sesion se;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etLoginUsuario = findViewById(R.id.etLoginUsuario);
        etLoginPassword = findViewById(R.id.etLoginPassword);

        activarPermisos();
        inicializarFirebase();

        se = new sesion(this);

        if(se.usuSesion!=null)
        {
            login(se.usuSesion.getCorreo(),se.usuSesion.getContrasena());
        }

    }

    public void onLogin(View view)
    {
        String stringLoginUsuario = etLoginUsuario.getText().toString();
        String stringLoginPassword = etLoginPassword.getText().toString();

        //
        if(!TextUtils.isEmpty(stringLoginUsuario) && !TextUtils.isEmpty(stringLoginPassword))
        {
            login(stringLoginUsuario,stringLoginPassword);


        }
        else
        {
            Toast.makeText(this,"Rellene los campos", Toast.LENGTH_SHORT).show();
        }
    }

    public void onCambiar(View view) {
        Intent i = new Intent(this, CrearCuenta.class);
        startActivity(i);
    }

    public void ingresarUsuario(usuario usu)
    {

        se.guardarSesion(usu);


        Intent i = new Intent(this,Inicio.class);
        startActivity(i);
        finish();
    }

    public void login(String stringLoginUsuario, String stringLoginPassword)
    {
        Query query = dr.child("Usuario").orderByChild("correo").equalTo(stringLoginUsuario);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists())
                {
                    for (DataSnapshot data : snapshot.getChildren()) {
                        usuario usu = data.getValue(usuario.class);

                        if(usu.getContrasena().equals(stringLoginPassword))
                        {
                            ingresarUsuario(usu);
                        }

                        break;
                    }
                }
                else
                {
                    Toast.makeText(getBaseContext(),"Usuario incorrecto", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getBaseContext(),"Error al consultar", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {

    }

}
package com.example.groupmanagerdam;

import androidx.annotation.NonNull;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.example.groupmanagerdam.models.tareaModelAdaptador;
import com.example.groupmanagerdam.utils.AdaptadorTarea;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class listar_tareas extends BaseMenu {

    public ListView listView_tareas;
    String uid_proyectoElegido;

    public ArrayList<tareaModelAdaptador> alTarea;
    public AdaptadorTarea adaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_tareas);

        listView_tareas = findViewById(R.id.listView_tareas);

        iniciarSesion();
        activarPermisos();
        inicializarFirebase();

        alTarea = new ArrayList<>();

        uid_proyectoElegido = getIntent().getStringExtra("uid_proyecto");

        Query query = dr.child("Proyecto").child(uid_proyectoElegido);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists())
                {
                    if(snapshot.hasChild("Tarea"))
                    {
                        listView_tareas.setAdapter(null);
                        alTarea.clear();

                        int maxCount = (int) snapshot.child("Tarea").getChildrenCount();
                        int count = 0;

                        for(DataSnapshot dsn : snapshot.child("Tarea").getChildren())
                        {
                            tareaModelAdaptador t = dsn.getValue(tareaModelAdaptador.class);
                            buscarUsuario(t,maxCount,count);
                            count++;
                        }

                    }

                }
                else
                {
                    Toast.makeText(getBaseContext(),"No se ha encontrado informacion", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getBaseContext(),"Error al consultar", Toast.LENGTH_SHORT).show();
            }
        });

        listView_tareas.setOnItemClickListener((parent, view, position, id) -> {
            tareaModelAdaptador t = alTarea.get(position);


            Intent i = new Intent(this, modificar_tareas.class);
            i.putExtra("uid_proyecto", uid_proyectoElegido);
            i.putExtra("uid_tarea", t.getUid_tarea());

            startActivity(i);

        });

    }

    public void buscarUsuario(tareaModelAdaptador t, int maxCount, int count)
    {
        dr.child("Usuario").child(t.getUid_usuario()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists())
                {
                    String nombre = snapshot.child("nombre").getValue().toString();
                    t.setNombreIntegrante(nombre);
                    alTarea.add(t);

                    if(maxCount-1 == count)
                    {

                        adaptador = new AdaptadorTarea(getBaseContext(), alTarea);
                        listView_tareas.setAdapter(adaptador);
                    }


                }
                else
                {
                    Toast.makeText(getBaseContext(),"No se ha encontrado informacion", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getBaseContext(),"Error al consultar", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onCrearTarea(View view)
    {
        Query query = dr.child("Proyecto").child(uid_proyectoElegido);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
             @Override
             public void onDataChange(@NonNull DataSnapshot snapshot) {
                 if(snapshot.exists())
                 {
                     if(snapshot.hasChild("listaIntegrante"))
                     {

                         int maxCount = (int) snapshot.child("listaIntegrante").getChildrenCount();

                         if(maxCount>0)
                         {


                             boolean val = false;

                             for(DataSnapshot dsn : snapshot.child("listaIntegrante").getChildren()) {
                                boolean dataDsn = (boolean) dsn.child("confirmacion").getValue();

                                if(dataDsn)
                                {
                                    val = true;
                                    break;
                                }
                             }

                             if(val)
                             {
                                 irCrearTareas();
                             }
                             else
                             {
                                 Toast.makeText(getBaseContext(),"No cuenta con participantes confirmados", Toast.LENGTH_SHORT).show();
                             }
                         }
                         else
                         {


                             Toast.makeText(getBaseContext(),"No cuenta con la cantidad de participantes", Toast.LENGTH_SHORT).show();
                         }

                     }
                     else
                     {
                         Toast.makeText(getBaseContext(),"No cuenta con la cantidad de participantes", Toast.LENGTH_SHORT).show();
                     }

                 }
                 else
                 {
                     Toast.makeText(getBaseContext(),"No se ha encontrado informacion", Toast.LENGTH_SHORT).show();
                 }
             }

             @Override
             public void onCancelled(@NonNull DatabaseError error) {
                 Toast.makeText(getBaseContext(),"Error al consultar", Toast.LENGTH_SHORT).show();
             }
         });

    }

    public void irCrearTareas()
    {
        Intent i = new Intent(this, crear_tareas.class);
        i.putExtra("uid_proyecto", uid_proyectoElegido);
        startActivity(i);
    }
}
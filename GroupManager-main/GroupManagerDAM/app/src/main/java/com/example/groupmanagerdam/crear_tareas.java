package com.example.groupmanagerdam;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.groupmanagerdam.models.integrante;
import com.example.groupmanagerdam.models.proyecto;
import com.example.groupmanagerdam.models.tarea;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.UUID;

public class crear_tareas extends BaseMenu {

    String uid_proyectoElegido;

    public Spinner sp_integrantes;
    public TextView txt_nombreTarea, txt_descripcionTarea, txt_fechaInicioTarea, txt_fechaFinTarea;
    public ArrayList<String> listaUIDParticipantes;
    public ArrayList<String> listaSpinner;

    public int contador;
    public String[] arraySpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_tareas);

        sp_integrantes = findViewById(R.id.sp_integrantes);

        txt_nombreTarea = findViewById(R.id.txt_nombreTarea);
        txt_descripcionTarea = findViewById(R.id.txt_descripcionTarea);
        txt_fechaInicioTarea = findViewById(R.id.txt_fechaInicioTarea);
        txt_fechaFinTarea = findViewById(R.id.txt_fechaFinTarea);

        iniciarSesion();
        activarPermisos();
        inicializarFirebase();


        listaUIDParticipantes = new ArrayList<>();
        listaSpinner = new ArrayList<>();

        uid_proyectoElegido = getIntent().getStringExtra("uid_proyecto");

        Query query = dr.child("Proyecto").child(uid_proyectoElegido);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists())
                {
                    if(snapshot.hasChild("listaIntegrante")) {
                        DataSnapshot dsn = snapshot.child("listaIntegrante");

                        for (DataSnapshot d : dsn.getChildren()) {
                            integrante in = d.getValue(integrante.class);

                            if (in.getConfirmacion()) {
                                listaUIDParticipantes.add(in.getUid_usuario());
                            }
                        }

                        obtenerNombres();

                    }
                }
                else
                {
                    Toast.makeText(getBaseContext(),"No se ha encontrado informacion", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getBaseContext(),"Error al consultar", Toast.LENGTH_SHORT).show();
            }
        });


    }

    public void obtenerNombres()
    {
        arraySpinner = new String[listaUIDParticipantes.size()];

        contador = 0;
        for(String uidIntegrante : listaUIDParticipantes)
        {
            //System.out.println(uidIntegrante);
            dr.child("Usuario").child(uidIntegrante).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if(snapshot.exists())
                    {
                        String nombre = snapshot.child("nombre").getValue().toString();
                        //System.out.println(nombre);
                        arraySpinner[contador] = nombre;
                        contador++;

                        if(contador == listaUIDParticipantes.size())
                        {
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(),
                                    android.R.layout.simple_spinner_item, arraySpinner);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sp_integrantes.setAdapter(adapter);
                        }
                    }


                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Toast.makeText(getBaseContext(),"Error al consultar", Toast.LENGTH_SHORT).show();
                }
            });
        }



    }

    public void onCrearTarea(View view)
    {
        String stringNombreTarea = txt_nombreTarea.getText().toString();
        String stringDescripcionTarea = txt_descripcionTarea.getText().toString();
        String stringFechaInicioTarea = txt_fechaInicioTarea.getText().toString();
        String stringFechaFinTarea = txt_fechaFinTarea.getText().toString();

        int indexSpinner = sp_integrantes.getSelectedItemPosition();

        System.out.println(indexSpinner);

        if(!TextUtils.isEmpty(stringNombreTarea) && !TextUtils.isEmpty(stringDescripcionTarea) && !TextUtils.isEmpty(stringFechaInicioTarea)
                && !TextUtils.isEmpty(stringFechaFinTarea) && indexSpinner>=0 )
        {
            tarea t = new tarea();

            t.setUid_tarea(UUID.randomUUID().toString());
            t.setUid_usuario(listaUIDParticipantes.get(indexSpinner));
            t.setNombre(stringNombreTarea);
            t.setDescripcion(stringDescripcionTarea);
            t.setFecha_inicio(stringFechaInicioTarea);
            t.setFecha_fin(stringFechaFinTarea);
            t.setEstado("Pendiente");

            dr.child("Proyecto").child(uid_proyectoElegido).child("Tarea").child(t.getUid_tarea()).setValue(t);

            limpiarDatos();

            Toast.makeText(this,"Se ha registrado la tarea", Toast.LENGTH_SHORT).show();
        }
        else
        {
            Toast.makeText(this,"Rellene los campos", Toast.LENGTH_SHORT).show();
        }
    }

    public void limpiarDatos()
    {
        txt_nombreTarea.setText("");
        txt_descripcionTarea.setText("");
        txt_fechaInicioTarea.setText("");
        txt_fechaFinTarea.setText("");
    }
}
package com.example.groupmanagerdam.models;

public class integranteModelAdaptador extends integrante{
    public String nombre;
    public String correo;

    public integranteModelAdaptador() {

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
}

package com.example.groupmanagerdam;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.groupmanagerdam.models.integrante;
import com.example.groupmanagerdam.models.proyecto;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.UUID;

public class IngresarProyecto extends BaseMenu {

    public TextView etIngresarCodigo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingresar_proyecto);

        etIngresarCodigo = findViewById(R.id.etIngresarCodigo);


        iniciarSesion();
        activarPermisos();
        inicializarFirebase();


    }

    public void onRegistro(View view)
    {
        final String stringIngresarCodigo = etIngresarCodigo.getText().toString();

        if(!TextUtils.isEmpty(stringIngresarCodigo))
        {
            checkProyecto(stringIngresarCodigo);
        }
        else
        {
            Toast.makeText(this,"Rellene los campos", Toast.LENGTH_SHORT).show();
        }
    }

    public void limpiarCampos()
    {
        etIngresarCodigo.setText("");
    }

    public void registrarIntegrante(String stringIngresarCodigo)
    {
        integrante in = new integrante();

        in.setConfirmacion(false);
        in.setRol("Participante");
        in.setUid_proyecto(stringIngresarCodigo);
        in.setUid_usuario(usuSesion.getUid_usuario());

        dr.child("Proyecto").child(stringIngresarCodigo).child("listaIntegrante").child(in.getUid_usuario()).setValue(in);

        Toast.makeText(getBaseContext(),"Solicitud realizada, espere la confirmacion", Toast.LENGTH_SHORT).show();

        limpiarCampos();
    }

    public void checkProyecto(String stringIngresarCodigo)
    {
        Query query = dr.child("Proyecto").orderByChild("uid_proyecto").equalTo(stringIngresarCodigo);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists())
                {
                    for (DataSnapshot data : snapshot.getChildren()) {
                        proyecto pro = data.getValue(proyecto.class);

                        if(data.hasChild("listaIntegrante"))
                        {
                            if(data.child("listaIntegrante").hasChild(usuSesion.getUid_usuario()))
                            {
                                Toast.makeText(getBaseContext(),"Usuario ya registrado con anteroridad", Toast.LENGTH_SHORT).show();
                            }
                            else
                            {
                                registrarIntegrante(stringIngresarCodigo);
                            }

                        }
                        else
                        {
                            registrarIntegrante(stringIngresarCodigo);
                        }
                    }
                }
                else
                {
                    Toast.makeText(getBaseContext(),"Proyecto inexistente", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getBaseContext(),"Ha ocurrido un error", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
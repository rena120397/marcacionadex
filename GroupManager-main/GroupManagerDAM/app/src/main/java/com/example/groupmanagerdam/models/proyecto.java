package com.example.groupmanagerdam.models;

public class proyecto {
    private String nombre;
    private String descripcion;
    private String fecha_inicio;
    private String fecha_fin;
    private double presupuesto;
    private String uid_proyecto;
    private String uid_usuario;
    private String estado;

    public proyecto() {

    }

    public proyecto(String nombre, String descripcion, String fecha_inicio, String fecha_fin, double presupuesto, String uid_proyecto, String uid_usuario, String estado) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.fecha_inicio = fecha_inicio;
        this.fecha_fin = fecha_fin;
        this.presupuesto = presupuesto;
        this.uid_proyecto = uid_proyecto;
        this.uid_usuario = uid_usuario;
        this.estado = estado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(String fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public String getFecha_fin() {
        return fecha_fin;
    }

    public void setFecha_fin(String fecha_fin) {
        this.fecha_fin = fecha_fin;
    }

    public double getPresupuesto() {
        return presupuesto;
    }

    public void setPresupuesto(double presupuesto) {
        this.presupuesto = presupuesto;
    }

    public String getUid_proyecto() {
        return uid_proyecto;
    }

    public void setUid_proyecto(String uid_proyecto) {
        this.uid_proyecto = uid_proyecto;
    }

    public String getUid_usuario() {
        return uid_usuario;
    }

    public void setUid_usuario(String uid_usuario) {
        this.uid_usuario = uid_usuario;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}


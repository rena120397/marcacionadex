package com.example.groupmanagerdam.models;

public class integrante {


    private String uid_proyecto;
    private String uid_usuario;
    private String rol;
    private boolean confirmacion;

    public integrante() {

    }

    public String getUid_proyecto() {
        return uid_proyecto;
    }

    public void setUid_proyecto(String uid_proyecto) {
        this.uid_proyecto = uid_proyecto;
    }

    public String getUid_usuario() {
        return uid_usuario;
    }

    public void setUid_usuario(String uid_usuario) {
        this.uid_usuario = uid_usuario;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public boolean getConfirmacion() {
        return confirmacion;
    }

    public void setConfirmacion(boolean confirmacion) {
        this.confirmacion = confirmacion;
    }
}

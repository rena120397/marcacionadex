package com.example.groupmanagerdam;


import android.os.Bundle;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.groupmanagerdam.models.tarea;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.Arrays;

public class asignar_estado_tarea extends BaseMenu {

    public TextView txt_tareaNombreInvitado, txt_tareaDescripcionInvitado, txt_tareaFechaInicioInvitado, txt_tareaFechaFinInvitado;
    public Spinner sp_tareaEstadoInvitado;

    public String uid_proyectoElegido;
    public String uid_tareaElegida;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asignar_estado_tarea);

        txt_tareaNombreInvitado = findViewById(R.id.txt_tareaNombreInvitado);
        txt_tareaDescripcionInvitado = findViewById(R.id.txt_tareaDescripcionInvitado);
        txt_tareaFechaInicioInvitado = findViewById(R.id.txt_tareaFechaInicioInvitado);
        txt_tareaFechaFinInvitado = findViewById(R.id.txt_tareaFechaFinInvitado);
        sp_tareaEstadoInvitado = findViewById(R.id.sp_tareaEstadoInvitado);

        uid_proyectoElegido = getIntent().getStringExtra("uid_proyecto");
        uid_tareaElegida = getIntent().getStringExtra("uid_tarea");

        iniciarSesion();
        activarPermisos();
        inicializarFirebase();

        Query query = dr.child("Proyecto").child(uid_proyectoElegido);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists())
                { ;
                    if(snapshot.hasChild("Tarea") && snapshot.child("Tarea").hasChild(uid_tareaElegida))
                    {
                        tarea t = snapshot.child("Tarea").child(uid_tareaElegida).getValue(tarea.class);
                        txt_tareaNombreInvitado.setText(t.getNombre());
                        txt_tareaDescripcionInvitado.setText(t.getDescripcion());
                        txt_tareaFechaInicioInvitado.setText(t.getFecha_inicio());
                        txt_tareaFechaFinInvitado.setText(t.getFecha_fin());

                        String estado = t.getEstado();

                        String[] stringArray = getResources().getStringArray(R.array.listaEstados);

                        if (Arrays.asList(stringArray).contains(estado)) {
                            for(int i = 0;i<stringArray.length; i++)
                            {
                                if(stringArray[i].equalsIgnoreCase(estado))
                                {
                                    sp_tareaEstadoInvitado.setSelection(i);
                                    break;
                                }
                            }
                        }

                    }
                }
                else
                {
                    Toast.makeText(getBaseContext(),"No se ha encontrado informacion", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getBaseContext(),"Error al consultar", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void onEditar(View view)
    {
        Query query = dr.child("Proyecto").child(uid_proyectoElegido);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists())
                { ;
                    if(snapshot.hasChild("Tarea") && snapshot.child("Tarea").hasChild(uid_tareaElegida))
                    {
                        DatabaseReference tarea = dr.child("Proyecto").child(uid_proyectoElegido).child("Tarea").child(uid_tareaElegida);

                        String estado = sp_tareaEstadoInvitado.getSelectedItem().toString();

                        tarea.child("estado").setValue(estado);

                        Toast.makeText(getBaseContext(),"Se ha editado la tarea", Toast.LENGTH_SHORT).show();

                    }
                }
                else
                {
                    Toast.makeText(getBaseContext(),"No se ha encontrado informacion", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getBaseContext(),"Error al consultar", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
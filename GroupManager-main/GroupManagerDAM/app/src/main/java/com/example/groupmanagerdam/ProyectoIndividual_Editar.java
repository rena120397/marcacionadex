package com.example.groupmanagerdam;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.groupmanagerdam.models.proyecto;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.Arrays;

public class ProyectoIndividual_Editar extends BaseMenu {
    public TextView etEditarProyectoNombre, etEditarProyectoDescripcion, etEditarProyectoInicio, etEditarProyectoFin, etEditarProyectoPresupuesto;
    public Spinner etEditarProyectoEstado;

    public String uid_proyectoElegido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proyecto_individual__editar);

        etEditarProyectoNombre = findViewById(R.id.etEditarProyectoNombre);
        etEditarProyectoDescripcion = findViewById(R.id.etEditarProyectoDescripcion);
        etEditarProyectoInicio = findViewById(R.id.etEditarProyectoInicio);
        etEditarProyectoFin = findViewById(R.id.etEditarProyectoFin);
        etEditarProyectoPresupuesto = findViewById(R.id.etEditarProyectoPresupuesto);
        etEditarProyectoEstado = findViewById(R.id.etEditarProyectoEstado);

        iniciarSesion();
        activarPermisos();
        inicializarFirebase();

        uid_proyectoElegido = getIntent().getStringExtra("uid_proyecto");

        Query query = dr.child("Proyecto").orderByChild("uid_proyecto").equalTo(uid_proyectoElegido);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists())
                {
                    for (DataSnapshot data : snapshot.getChildren()) {
                        proyecto pro = data.getValue(proyecto.class);

                        etEditarProyectoNombre.setText(pro.getNombre());
                        etEditarProyectoDescripcion.setText(pro.getDescripcion());
                        etEditarProyectoInicio.setText(pro.getFecha_inicio());
                        etEditarProyectoFin.setText(pro.getFecha_fin());
                        etEditarProyectoPresupuesto.setText(pro.getPresupuesto()+"");

                        String estado = pro.getEstado();

                        String[] stringArray = getResources().getStringArray(R.array.listaEstados);

                        if (Arrays.asList(stringArray).contains(estado)) {
                            for(int i = 0;i<stringArray.length; i++)
                            {
                                if(stringArray[i].equalsIgnoreCase(estado))
                                {
                                    etEditarProyectoEstado.setSelection(i);
                                    break;
                                }
                            }
                        }

                        break;
                    }
                }
                else
                {
                    Toast.makeText(getBaseContext(),"No se ha encontrado informacion", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getBaseContext(),"Error al consultar", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void onEditar(View view)
    {
        String stringProyectoNombre = etEditarProyectoNombre.getText().toString();
        String stringProyectoDescripcion = etEditarProyectoDescripcion.getText().toString();
        String stringProyectoInicio = etEditarProyectoInicio.getText().toString();
        String stringProyectoFin = etEditarProyectoFin.getText().toString();
        String stringProyectoPresupuesto = etEditarProyectoPresupuesto.getText().toString();

        String stringEstado = etEditarProyectoEstado.getSelectedItem().toString();

        if(!TextUtils.isEmpty(stringProyectoNombre) && !TextUtils.isEmpty(stringProyectoDescripcion) && !TextUtils.isEmpty(stringProyectoPresupuesto)
                && !TextUtils.isEmpty(stringProyectoInicio) && !TextUtils.isEmpty(stringProyectoFin)) {

            dr.child("Proyecto").child(uid_proyectoElegido).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if(snapshot.exists())
                    {
                        DatabaseReference pro = dr.child("Proyecto").child(uid_proyectoElegido);
                        pro.child("nombre").setValue(stringProyectoNombre);
                        pro.child("descripcion").setValue(stringProyectoDescripcion);
                        pro.child("fecha_inicio").setValue(stringProyectoInicio);
                        pro.child("fecha_fin").setValue(stringProyectoFin);
                        pro.child("presupuesto").setValue(Double.parseDouble(stringProyectoPresupuesto));
                        pro.child("estado").setValue(stringEstado);

                        Toast.makeText(getBaseContext(),"Se ha editado el proyecto", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(getBaseContext(),"No se ha encontrado informacion", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Toast.makeText(getBaseContext(),"Error al consultar", Toast.LENGTH_SHORT).show();
                }
            });

        }
        else
        {
            Toast.makeText(this,"Rellene los campos", Toast.LENGTH_SHORT).show();
        }
    }

    public void onBackPressed() {
        super.onBackPressed();
    }
}
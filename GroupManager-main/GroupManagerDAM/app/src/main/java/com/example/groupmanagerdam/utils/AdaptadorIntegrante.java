package com.example.groupmanagerdam.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.groupmanagerdam.R;
import com.example.groupmanagerdam.models.integranteModelAdaptador;
import com.example.groupmanagerdam.models.proyecto;

import java.util.ArrayList;

public class AdaptadorIntegrante extends BaseAdapter {

    private Context context;
    private ArrayList<integranteModelAdaptador> alIntegrante;

    public AdaptadorIntegrante(Context context, ArrayList<integranteModelAdaptador> alIntegrante) {
        this.context = context;
        this.alIntegrante = alIntegrante;
    }

    @Override
    public int getCount() {
        return alIntegrante.size();
    }

    @Override
    public Object getItem(int position) {
        return alIntegrante.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        integranteModelAdaptador in = (integranteModelAdaptador) getItem(position);

        convertView = LayoutInflater.from(context).inflate(R.layout.itemparticipantes,null);

        TextView txtNombre = convertView.findViewById(R.id.textNombre);
        TextView txtRol = convertView.findViewById(R.id.txtRol);
        TextView txtConfirmacion = convertView.findViewById(R.id.txtConfirmacion);

        txtNombre.setText(in.getNombre());
        txtRol.setText(in.getRol());
        
        String texto = (in.getConfirmacion())? "Si" :  "No";

        txtConfirmacion.setText(texto);

        return convertView;
    }
}

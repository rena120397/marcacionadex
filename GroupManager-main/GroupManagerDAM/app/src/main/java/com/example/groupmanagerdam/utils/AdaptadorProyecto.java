package com.example.groupmanagerdam.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.groupmanagerdam.R;
import com.example.groupmanagerdam.models.proyecto;

import java.util.ArrayList;

public class AdaptadorProyecto extends BaseAdapter {

    private Context context;
    private ArrayList<proyecto> alProyectos;

    public AdaptadorProyecto(Context context, ArrayList<proyecto> alProyectos) {
        this.context = context;
        this.alProyectos = alProyectos;
    }

    @Override
    public int getCount() {
        return alProyectos.size();
    }

    @Override
    public Object getItem(int position) {
        return alProyectos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        proyecto pro = (proyecto) getItem(position);

        convertView = LayoutInflater.from(context).inflate(R.layout.itemproyecto,null);

        TextView txtNombre = convertView.findViewById(R.id.textNombre);
        TextView txtDescripcion = convertView.findViewById(R.id.textDescripcion);

        txtNombre.setText(pro.getNombre());
        txtDescripcion.setText(pro.getDescripcion());

        return convertView;
    }
}

package com.example.groupmanagerdam;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.Arrays;

public class Editar_Participante extends BaseMenu {

    public TextView txtNombreParticipante, txtCorreoParticipante;

    public Spinner spinnerRolParticipante;
    public Switch switchConfirmacionParticipante;

    public String uid_proyectoElegido, uid_usuarioElegido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar__participante);

        txtNombreParticipante = findViewById(R.id.txtNombreParticipante);
        txtCorreoParticipante = findViewById(R.id.txtCorreoParticipante);
        spinnerRolParticipante = findViewById(R.id.spinnerRolParticipante);
        switchConfirmacionParticipante = findViewById(R.id.switchConfirmacionParticipante);

        uid_proyectoElegido = getIntent().getStringExtra("uid_proyecto");
        uid_usuarioElegido = getIntent().getStringExtra("uid_usuario");

        iniciarSesion();
        activarPermisos();
        inicializarFirebase();

        Query query = dr.child("Proyecto").child(uid_proyectoElegido);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists())
                {
                    if(snapshot.hasChild("listaIntegrante") && snapshot.child("listaIntegrante").hasChild(uid_usuarioElegido))
                    {
                        DataSnapshot dPar= snapshot.child("listaIntegrante").child(uid_usuarioElegido);

                        boolean confirmacion = (boolean) dPar.child("confirmacion").getValue();
                        String rol = (String) dPar.child("rol").getValue();

                        switchConfirmacionParticipante.setChecked(confirmacion);

                        String[] stringArray = getResources().getStringArray(R.array.listaRolesParticipante);

                        if (Arrays.asList(stringArray).contains(rol)) {
                            for(int i = 0;i<stringArray.length; i++)
                            {
                                if(stringArray[i].equalsIgnoreCase(rol))
                                {
                                    spinnerRolParticipante.setSelection(i);
                                    break;
                                }
                            }
                        }

                    }
                }
                else
                {
                    Toast.makeText(getBaseContext(),"No se ha encontrado informacion", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getBaseContext(),"Error al consultar", Toast.LENGTH_SHORT).show();
            }
        });


        Query query2 = dr.child("Usuario").child(uid_usuarioElegido);
        query2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists())
                {
                    String correo = (String) snapshot.child("correo").getValue();
                    String nombre = (String) snapshot.child("nombre").getValue();

                    txtCorreoParticipante.setText("Correo : "+correo);
                    txtNombreParticipante.setText("Nombre : "+nombre);
                }
                else
                {
                    Toast.makeText(getBaseContext(),"No se ha encontrado informacion", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getBaseContext(),"Error al consultar", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void onEliminar(View view)
    {
        AlertDialog dialog = new AlertDialog.Builder(this).setMessage(R.string.txt_dialog_borrar_participante)
                .setPositiveButton(R.string.txt_si, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        eliminarParticipante();

                        finish();
                    }
                }).setNegativeButton(R.string.txt_no, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        // Do stuff when user neglects.
                    }
                }).setOnCancelListener(new DialogInterface.OnCancelListener() {

                    @Override
                    public void onCancel(DialogInterface dialog) {
                        dialog.dismiss();
                        // Do stuff when cancelled
                    }
                }).create();
        dialog.show();
    }

    public void eliminarParticipante()
    {
        Query query = dr.child("Proyecto").child(uid_proyectoElegido);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists())
                {
                    if(snapshot.hasChild("listaIntegrante") && snapshot.child("listaIntegrante").hasChild(uid_usuarioElegido))
                    {
                        dr.child("Proyecto").child(uid_proyectoElegido).child("listaIntegrante").child(uid_usuarioElegido).removeValue();
                        Toast.makeText(getBaseContext(),"Se ha borrado el usuario", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(getBaseContext(),"No se ha encontrado informacion", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getBaseContext(),"Error al consultar", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onEditar(View view)
    {
        Query query = dr.child("Proyecto").child(uid_proyectoElegido);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists())
                {
                    if(snapshot.hasChild("listaIntegrante") && snapshot.child("listaIntegrante").hasChild(uid_usuarioElegido))
                    {
                        String rol = spinnerRolParticipante.getSelectedItem().toString();
                        boolean confirmacion = switchConfirmacionParticipante.isChecked();

                        DatabaseReference participante = dr.child("Proyecto").child(uid_proyectoElegido).child("listaIntegrante").child(uid_usuarioElegido);
                        participante.child("rol").setValue(rol);
                        participante.child("confirmacion").setValue(confirmacion);
                        Toast.makeText(getBaseContext(),"Se ha editado el usuario", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(getBaseContext(),"No se ha encontrado informacion", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getBaseContext(),"Error al consultar", Toast.LENGTH_SHORT).show();
            }
        });


    }
}
package com.example.groupmanagerdam.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.groupmanagerdam.R;
import com.example.groupmanagerdam.models.tareaModelAdaptador;

import java.util.ArrayList;

public class AdaptadorTarea extends BaseAdapter {
    private Context context;
    private ArrayList<tareaModelAdaptador> alTarea;

    public AdaptadorTarea(Context context, ArrayList<tareaModelAdaptador> alTarea) {
        this.context = context;
        this.alTarea = alTarea;
    }

    @Override
    public int getCount() {
        return alTarea.size();
    }

    @Override
    public Object getItem(int position) {
        return alTarea.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        tareaModelAdaptador tar = (tareaModelAdaptador) getItem(position);

        convertView = LayoutInflater.from(context).inflate(R.layout.itemtarea,null);

        TextView txtNombreIntegrante = convertView.findViewById(R.id.textNombreIntegrante);
        TextView txtNombre = convertView.findViewById(R.id.textNombre);
        TextView txtDescripcion = convertView.findViewById(R.id.textDescripcion);
        TextView txtEstado = convertView.findViewById(R.id.textEstado);

        txtNombreIntegrante.setText(tar.getNombreIntegrante());
        txtNombre.setText(tar.getNombre());
        txtDescripcion.setText(tar.getDescripcion());
        txtEstado.setText(tar.getEstado());

        return convertView;
    }
}

package com.example.groupmanagerdam.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.example.groupmanagerdam.models.usuario;
import com.google.gson.Gson;

public class sesion {

    private final SharedPreferences preferences;
    public usuario usuSesion;

    public sesion(Context context)
    {
        preferences = context.getSharedPreferences("PREF_GROUPMANAGER", context.MODE_PRIVATE);

        obtenerUsuario();


    }

    private String loadSesion()
    {
        return preferences.getString("Usuario", "");
    }

    public boolean checkSesion()
    {
        return usuSesion!=null;
    }

    private void obtenerUsuario()
    {
        String json = loadSesion();

        Gson gson = new Gson();

        usuSesion =  (!TextUtils.isEmpty(json))? gson.fromJson(json, usuario.class) : null;
    }

    public usuario getSesion()
    {
        return usuSesion;
    }

    public void guardarSesion(usuario usu)
    {
        String json = new Gson().toJson(usu);

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("Usuario", json);

        editor.apply();

    }

    public void borrarSesion()
    {
        usuSesion = null;

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("Usuario", "");

        editor.apply();
    }


}

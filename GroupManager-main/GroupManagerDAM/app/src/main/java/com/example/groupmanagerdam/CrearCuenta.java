package com.example.groupmanagerdam;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.groupmanagerdam.models.usuario;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.UUID;

public class CrearCuenta extends BaseFirebase {

    public TextView etCrearCuentaNombre, etCrearCuentaCorreo, etCrearCuentaPass;
    public Spinner etElegirRol;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_cuenta);


        etCrearCuentaNombre = findViewById(R.id.etCrearCuentaNombre);
        etCrearCuentaCorreo = findViewById(R.id.etCrearCuentaCorreo);
        etCrearCuentaPass = findViewById(R.id.etCrearCuentaPass);
        etElegirRol = findViewById(R.id.etElegirRol);

        activarPermisos();
        inicializarFirebase();
    }

    public void onRegistrar(View view)
    {
        String stringCuentaNombre = etCrearCuentaNombre.getText().toString();
        String stringCuentaCorreo = etCrearCuentaCorreo.getText().toString();
        String stringCuentaPass = etCrearCuentaPass.getText().toString();
        String stringElegirRol = etElegirRol.getSelectedItem().toString();

        if(!TextUtils.isEmpty(stringCuentaNombre) && !TextUtils.isEmpty(stringCuentaNombre) && !TextUtils.isEmpty(stringCuentaNombre))
        {

            Query query = dr.child("Usuario").orderByChild("correo").equalTo(stringCuentaCorreo);

            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if(snapshot.exists())
                    {
                        Toast.makeText(getBaseContext(),"Correo ya utilizado", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        registrarUsuario(stringCuentaNombre, stringCuentaCorreo, stringCuentaPass, stringElegirRol);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Toast.makeText(getBaseContext(),"Ha ocurrido un error", Toast.LENGTH_SHORT).show();
                }
            });


        }
        else
        {
            Toast.makeText(this,"Rellene los campos", Toast.LENGTH_SHORT).show();
        }
    }

    public void limpiarDatos()
    {
        etCrearCuentaNombre.setText("");
        etCrearCuentaCorreo.setText("");
        etCrearCuentaPass.setText("");
    }

    public void registrarUsuario(String stringCuentaNombre, String stringCuentaCorreo, String stringCuentaPass, String stringElegirRol)
    {
        usuario u = new usuario();

        u.setUid_usuario(UUID.randomUUID().toString());
        u.setNombre(stringCuentaNombre);
        u.setCorreo(stringCuentaCorreo);
        u.setContrasena(stringCuentaPass);
        u.setRol(stringElegirRol);


        dr.child("Usuario").child(u.getUid_usuario()).setValue(u);

        limpiarDatos();

        Toast.makeText(this,"Bienvenido, se ha registrado correctamente", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
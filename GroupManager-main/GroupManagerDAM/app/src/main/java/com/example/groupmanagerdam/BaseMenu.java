package com.example.groupmanagerdam;

import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;

import com.example.groupmanagerdam.models.usuario;
import com.example.groupmanagerdam.utils.sesion;

public class BaseMenu extends BaseFirebase {

    protected usuario usuSesion;

    public boolean onCreateOptionsMenu(Menu menu) {

        iniciarSesion();

        if(usuSesion.getRol().equalsIgnoreCase("Colaborador"))
        {
            getMenuInflater().inflate(R.menu.menuapp_invitado, menu);
        }
        else
        {
            getMenuInflater().inflate(R.menu.menuapp_gestor, menu);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        sesion se = iniciarSesion();

        switch (id) {

            case R.id.item_listar_invitado: {
                Intent i = new Intent(this, Inicio.class);
                startActivity(i);
                break;
            }
            case R.id.item_unirse_invitado: {
                Intent i = new Intent(this, IngresarProyecto.class);
                startActivity(i);
                break;
            }
            case R.id.item_salir_invitado: {
                finishAffinity();
                System.exit(0);
            }
            case R.id.item_logout_invitado: {

                se.borrarSesion();

                Intent i = new Intent(this, MainActivity.class);
                startActivity(i);
                break;
            }
            case R.id.item_editar_invitado :
            {
                Intent i = new Intent(this, configurarUsuario.class);
                startActivity(i);
                break;
            }

            case R.id.item_listar_gestor: {
                Intent i = new Intent(this, Inicio.class);
                startActivity(i);
                break;
            }
            case R.id.item_crear_gestor: {
                Intent i = new Intent(this, NuevoProyecto.class);
                startActivity(i);
                break;
            }
            case R.id.item_salir_gestor: {
                finishAffinity();
                System.exit(0);
            }
            case R.id.item_logout_gestor: {

                se.borrarSesion();

                Intent i = new Intent(this, MainActivity.class);
                startActivity(i);
                break;
            }
            case R.id.item_editar_gestor :
            {
                Intent i = new Intent(this, configurarUsuario.class);
                startActivity(i);
                break;
            }

        }

        return super.onOptionsItemSelected(item);
    }

    public sesion iniciarSesion()
    {
        sesion se = new sesion(this);

        if(se.checkSesion())
        {
            usuSesion = se.getSesion();
        }
        else
        {
            regresarInicio();
        }

        return se;
    }

    public void regresarInicio()
    {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    @Override
    public void onRestart()
    {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }

}

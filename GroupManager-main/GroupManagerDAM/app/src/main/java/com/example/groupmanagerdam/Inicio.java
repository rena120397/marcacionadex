package com.example.groupmanagerdam;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.groupmanagerdam.models.integrante;
import com.example.groupmanagerdam.models.proyecto;
import com.example.groupmanagerdam.models.usuario;
import com.example.groupmanagerdam.utils.AdaptadorProyecto;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Objects;

public class Inicio extends BaseMenu {

    public ListView listView_proyectos;
    public ArrayList<proyecto> alProyectos;
    public AdaptadorProyecto adaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        listView_proyectos = findViewById(R.id.listView_proyectos);


        iniciarSesion();
        activarPermisos();
        inicializarFirebase();

        alProyectos = new ArrayList<>();

        if(usuSesion.getRol().equalsIgnoreCase("Colaborador"))
        {
            cargarDataInvitado();
        }
        else
        {
            cargarDataGestorProyecto();
        }

    }

    public void cargarDataInvitado()
    {
        Query query = dr.child("Proyecto").orderByChild("listaIntegrante");

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists())
                {

                    listView_proyectos.setAdapter(null);
                    alProyectos.clear();


                    for (DataSnapshot data : snapshot.getChildren()) {

                        if(data.hasChild("listaIntegrante") && data.child("listaIntegrante").hasChild(usuSesion.getUid_usuario()))
                        {
                            boolean val = (boolean) data.child("listaIntegrante").child(usuSesion.getUid_usuario()).child("confirmacion").getValue();

                            if(val)
                            {
                                proyecto pro = data.getValue(proyecto.class);
                                alProyectos.add(pro);
                            }
                        }

                    }

                    if(alProyectos.size()<=0)
                    {
                        Toast.makeText(getBaseContext(),"No se ha encontrado informacion", Toast.LENGTH_SHORT).show();
                    }


                    adaptador = new AdaptadorProyecto(getBaseContext(), alProyectos);
                    listView_proyectos.setAdapter(adaptador);


                }
                else
                {
                    Toast.makeText(getBaseContext(),"No se ha encontrado informacion", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getBaseContext(),"Error al consultar", Toast.LENGTH_SHORT).show();
            }
        });

        listView_proyectos.setOnItemClickListener((parent, view, position, id) -> {

            Intent i = new Intent(this, Proyecto_Individual_Invitado.class);
            i.putExtra("uid_proyecto", alProyectos.get(position).getUid_proyecto());
            startActivity(i);



            System.out.println(alProyectos.get(position).getUid_proyecto());
        });
    }



    public void cargarDataGestorProyecto()
    {
        Query query = dr.child("Proyecto").orderByChild("uid_usuario").equalTo(this.usuSesion.getUid_usuario());

        query.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists())
                {
                    listView_proyectos.setAdapter(null);
                    alProyectos.clear();

                    for (DataSnapshot data : snapshot.getChildren()) {
                        proyecto pro = data.getValue(proyecto.class);

                        alProyectos.add(pro);

                    }

                    adaptador = new AdaptadorProyecto(getBaseContext(), alProyectos);
                    listView_proyectos.setAdapter(adaptador);


                }
                else
                {
                    Toast.makeText(getBaseContext(),"No se ha encontrado informacion", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getBaseContext(),"Error al consultar", Toast.LENGTH_SHORT).show();
            }
        });


        listView_proyectos.setOnItemClickListener((parent, view, position, id) -> {

            Intent i = new Intent(this, ProyectoIndividual.class);
            i.putExtra("uid_proyecto", alProyectos.get(position).getUid_proyecto());
            startActivity(i);
        });
    }

    @Override
    public void onBackPressed() {

    }
}
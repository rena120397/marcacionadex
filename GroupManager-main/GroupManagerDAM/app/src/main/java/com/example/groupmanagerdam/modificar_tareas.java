package com.example.groupmanagerdam;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.groupmanagerdam.models.tarea;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class modificar_tareas extends BaseMenu {

    String uid_proyectoElegido;
    String uid_tareaElegida;

    public TextView txt_editarNombreTarea, txt_editarDescripcionTarea, txt_editarFechaInicioTarea, txt_editarFechaFinTarea;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar_tareas);

        txt_editarNombreTarea = findViewById(R.id.txt_editarNombreTarea);
        txt_editarDescripcionTarea = findViewById(R.id.txt_editarDescripcionTarea);
        txt_editarFechaInicioTarea = findViewById(R.id.txt_editarFechaInicioTarea);
        txt_editarFechaFinTarea = findViewById(R.id.txt_editarFechaFinTarea);

        iniciarSesion();
        activarPermisos();
        inicializarFirebase();

        uid_proyectoElegido = getIntent().getStringExtra("uid_proyecto");
        uid_tareaElegida = getIntent().getStringExtra("uid_tarea");

        Query query = dr.child("Proyecto").child(uid_proyectoElegido);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists())
                {
                    if(snapshot.hasChild("Tarea") && snapshot.child("Tarea").hasChild(uid_tareaElegida))
                    {
                        tarea t = snapshot.child("Tarea").child(uid_tareaElegida).getValue(tarea.class);
                        txt_editarNombreTarea.setText(t.getNombre());
                        txt_editarDescripcionTarea.setText(t.getDescripcion());
                        txt_editarFechaInicioTarea.setText(t.getFecha_inicio());
                        txt_editarFechaFinTarea.setText(t.getFecha_fin());

                    }
                }
                else
                {
                    Toast.makeText(getBaseContext(),"No se ha encontrado informacion", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getBaseContext(),"Error al consultar", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onEditar(View view)
    {
        String stringNombreTarea = txt_editarNombreTarea.getText().toString();
        String stringDescripcionTarea = txt_editarDescripcionTarea.getText().toString();
        String stringFechaInicioTarea = txt_editarFechaInicioTarea.getText().toString();
        String stringFechaFinTarea = txt_editarFechaFinTarea.getText().toString();

        if(!TextUtils.isEmpty(stringNombreTarea) && !TextUtils.isEmpty(stringDescripcionTarea) && !TextUtils.isEmpty(stringFechaInicioTarea)
                && !TextUtils.isEmpty(stringFechaFinTarea))
        {
            Query query = dr.child("Proyecto").child(uid_proyectoElegido);

            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if(snapshot.exists())
                    {
                        if(snapshot.hasChild("Tarea") && snapshot.child("Tarea").hasChild(uid_tareaElegida))
                        {
                            DatabaseReference tarea = dr.child("Proyecto").child(uid_proyectoElegido).child("Tarea").child(uid_tareaElegida);

                            tarea.child("nombre").setValue(stringNombreTarea);
                            tarea.child("descripcion").setValue(stringDescripcionTarea);
                            tarea.child("fecha_inicio").setValue(stringFechaInicioTarea);
                            tarea.child("fecha_fin").setValue(stringFechaFinTarea);

                            Toast.makeText(getBaseContext(),"Se ha editado la tarea", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Toast.makeText(getBaseContext(),"No se ha encontrado informacion", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Toast.makeText(getBaseContext(),"Error al consultar", Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
        {
            Toast.makeText(this,"Rellene los campos", Toast.LENGTH_SHORT).show();
        }
    }

    public void onBorrar(View view)
    {
        AlertDialog dialog = new AlertDialog.Builder(this).setMessage(R.string.txt_dialog_borrar_tarea)
                .setPositiveButton(R.string.txt_si, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        eliminarTarea();

                        finish();
                    }
                }).setNegativeButton(R.string.txt_no, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        // Do stuff when user neglects.
                    }
                }).setOnCancelListener(new DialogInterface.OnCancelListener() {

                    @Override
                    public void onCancel(DialogInterface dialog) {
                        dialog.dismiss();
                        // Do stuff when cancelled
                    }
                }).create();
        dialog.show();



    }

    public void eliminarTarea()
    {
        Query query = dr.child("Proyecto").child(uid_proyectoElegido);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists())
                {
                    if(snapshot.hasChild("Tarea") && snapshot.child("Tarea").hasChild(uid_tareaElegida))
                    {
                        dr.child("Proyecto").child(uid_proyectoElegido).child("Tarea").child(uid_tareaElegida).removeValue();
                        Toast.makeText(getBaseContext(),"Se ha borrado la tarea", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(getBaseContext(),"No se ha encontrado informacion", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getBaseContext(),"Error al consultar", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
package com.example.groupmanagerdam;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.groupmanagerdam.models.proyecto;

import java.util.UUID;

public class NuevoProyecto extends BaseMenu {

    public TextView etNuevoProyectoNombre, etNuevoProyectoDescripcion, etNuevoProyectoPresupuesto, etNuevoProyectoFin, etNuevoProyectoInicio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_proyecto);

        etNuevoProyectoNombre = findViewById(R.id.etEditarProyectoNombre);
        etNuevoProyectoDescripcion = findViewById(R.id.etEditarProyectoDescripcion);
        etNuevoProyectoPresupuesto = findViewById(R.id.etEditarProyectoPresupuesto);
        etNuevoProyectoFin = findViewById(R.id.etEditarProyectoFin);
        etNuevoProyectoInicio = findViewById(R.id.etEditarProyectoInicio);

        iniciarSesion();

        activarPermisos();
        inicializarFirebase();
    }

    public void onRegistrar(View view)
    {
        String stringProyectoNombre = etNuevoProyectoNombre.getText().toString();
        String stringProyectoDescripcion = etNuevoProyectoDescripcion.getText().toString();
        String stringProyectoPresupuesto = etNuevoProyectoPresupuesto.getText().toString();
        String stringProyectoInicio= etNuevoProyectoInicio.getText().toString();
        String stringProyectoFin = etNuevoProyectoFin.getText().toString();

        if(!TextUtils.isEmpty(stringProyectoNombre) && !TextUtils.isEmpty(stringProyectoDescripcion) && !TextUtils.isEmpty(stringProyectoPresupuesto)
                && !TextUtils.isEmpty(stringProyectoInicio) && !TextUtils.isEmpty(stringProyectoFin))
        {

            proyecto pro = new proyecto();
            pro.setUid_proyecto(UUID.randomUUID().toString());
            pro.setNombre(stringProyectoNombre);
            pro.setDescripcion(stringProyectoDescripcion);
            pro.setPresupuesto(Double.parseDouble(stringProyectoPresupuesto));
            pro.setFecha_inicio(stringProyectoInicio);
            pro.setFecha_fin(stringProyectoFin);
            pro.setUid_usuario(this.usuSesion.getUid_usuario());
            pro.setEstado("Pendiente");

            dr.child("Proyecto").child(pro.getUid_proyecto()).setValue(pro);

            limpiarDatos();

            Toast.makeText(this,"Se ha registrado el proyecto", Toast.LENGTH_SHORT).show();
        }
        else
        {
            Toast.makeText(this,"Rellene los campos", Toast.LENGTH_SHORT).show();
        }
    }

    public void limpiarDatos()
    {
        etNuevoProyectoNombre.setText("");
        etNuevoProyectoDescripcion.setText("");
        etNuevoProyectoPresupuesto.setText("");
        etNuevoProyectoFin.setText("");
        etNuevoProyectoInicio.setText("");
    }
}
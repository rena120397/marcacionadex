package com.example.groupmanagerdam;

import androidx.annotation.NonNull;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.example.groupmanagerdam.models.tareaModelAdaptador;
import com.example.groupmanagerdam.utils.AdaptadorTarea;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class listar_tareas_invitado extends BaseMenu {

    public ListView listView_tareas_invitado;

    public String uid_proyectoElegido;

    public ArrayList<tareaModelAdaptador> alTarea;
    public AdaptadorTarea adaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_tareas_invitado);

        listView_tareas_invitado = findViewById(R.id.listView_tareas_invitado);

        uid_proyectoElegido = getIntent().getStringExtra("uid_proyecto");

        iniciarSesion();
        activarPermisos();
        inicializarFirebase();

        alTarea = new ArrayList<>();

        Query query = dr.child("Proyecto").child(uid_proyectoElegido);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists())
                {
                    if(snapshot.hasChild("Tarea"))
                    {
                        listView_tareas_invitado.setAdapter(null);
                        alTarea.clear();

                        for(DataSnapshot dsn : snapshot.child("Tarea").getChildren())
                        {
                            String uid = dsn.child("uid_usuario").getValue().toString();

                            if(uid.equalsIgnoreCase(usuSesion.getUid_usuario()))
                            {
                                tareaModelAdaptador t = dsn.getValue(tareaModelAdaptador.class);
                                t.setNombreIntegrante(usuSesion.getNombre());
                                alTarea.add(t);
                            }
                        }

                        adaptador = new AdaptadorTarea(getBaseContext(), alTarea);
                        listView_tareas_invitado.setAdapter(adaptador);
                    }
                }
                else
                {
                    Toast.makeText(getBaseContext(),"No se ha encontrado informacion", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getBaseContext(),"Error al consultar", Toast.LENGTH_SHORT).show();
            }
        });

        listView_tareas_invitado.setOnItemClickListener((parent, view, position, id) ->
        {
            tareaModelAdaptador t = alTarea.get(position);


            Intent i = new Intent(this, asignar_estado_tarea.class);
            i.putExtra("uid_proyecto", uid_proyectoElegido);
            i.putExtra("uid_tarea", t.getUid_tarea());

            startActivity(i);
        });



    }
}
package com.example.groupmanagerdam;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.groupmanagerdam.models.integrante;
import com.example.groupmanagerdam.models.integranteModelAdaptador;
import com.example.groupmanagerdam.models.proyecto;
import com.example.groupmanagerdam.utils.AdaptadorIntegrante;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Objects;

public class Proyecto_Individual_Invitado extends BaseMenu {

    public TextView txt_nombre_pii, txt_descripcion_pii, txt_inicio_pii, txt_fin_pii, txt_presupuesto_pii, txt_creador_pii, txt_estado_pii;
    public ListView listIntegrantes_pii;

    String uid_proyectoElegido;
    public ArrayList<integranteModelAdaptador> alIntegranteA;
    public AdaptadorIntegrante adaptador;

    public int contador = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proyecto__individual__invitado);

        txt_nombre_pii = findViewById(R.id.txt_nombre_pii);
        txt_descripcion_pii = findViewById(R.id.txt_descripcion_pii);
        txt_inicio_pii = findViewById(R.id.txt_inicio_pii);
        txt_fin_pii = findViewById(R.id.txt_fin_pii);
        txt_presupuesto_pii = findViewById(R.id.txt_presupuesto_pii);
        txt_creador_pii = findViewById(R.id.txt_creador_pii);

        txt_estado_pii = findViewById(R.id.txt_estado_pii);

        listIntegrantes_pii = findViewById(R.id.listIntegrantes_pii);

        iniciarSesion();
        activarPermisos();
        inicializarFirebase();

        uid_proyectoElegido = getIntent().getStringExtra("uid_proyecto");

        Query query = dr.child("Proyecto").orderByChild("uid_proyecto").equalTo(uid_proyectoElegido);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists())
                {
                    for (DataSnapshot data : snapshot.getChildren()) {
                        proyecto pro = data.getValue(proyecto.class);

                        txt_nombre_pii.setText(("Nombre : " + pro.getNombre()));
                        txt_descripcion_pii.setText(("Descripcion : "+pro.getDescripcion()));
                        txt_inicio_pii.setText(("Fecha inicio : " + pro.getFecha_inicio()));
                        txt_fin_pii.setText(("Fecha fin : " + pro.getFecha_fin()));
                        txt_presupuesto_pii.setText(("Presupuesto : " + pro.getPresupuesto()));
                        txt_estado_pii.setText(("Estado : " + pro.getEstado()));


                        setCreador(pro.getUid_usuario());

                        break;
                    }
                }
                else
                {
                    Toast.makeText(getBaseContext(),"No se ha encontrado informacion", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getBaseContext(),"Error al consultar", Toast.LENGTH_SHORT).show();
            }
        });



        alIntegranteA = new ArrayList<>();

        Query query2 = dr.child("Proyecto").child(uid_proyectoElegido);

        query2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists())
                {
                    listIntegrantes_pii.setAdapter(null);
                    alIntegranteA.clear();

                    if(snapshot.hasChild("listaIntegrante"))
                    {
                        contador = 0;
                        final int maxCount = (int) snapshot.child("listaIntegrante").getChildrenCount();

                        if(maxCount<=0)
                        {
                            Toast.makeText(getBaseContext(),"No se ha encontrado informacion", Toast.LENGTH_SHORT).show();
                        }

                        for (DataSnapshot data : snapshot.child("listaIntegrante").getChildren())
                        {
                            integrante integr = data.getValue(integrante.class);

                            String uid_usuario = integr.getUid_usuario();

                            dr.child("Usuario").child(uid_usuario).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                    if(snapshot.exists())
                                    {
                                        integranteModelAdaptador intma = new integranteModelAdaptador();

                                        intma.setCorreo(Objects.requireNonNull(snapshot.child("correo").getValue()).toString());
                                        intma.setNombre(Objects.requireNonNull(snapshot.child("nombre").getValue()).toString());
                                        intma.setConfirmacion(integr.getConfirmacion());
                                        intma.setRol(integr.getRol());
                                        intma.setUid_proyecto(integr.getUid_proyecto());
                                        intma.setUid_usuario(uid_usuario);

                                        alIntegranteA.add(intma);

                                        contador++;

                                        if(contador>=maxCount)
                                        {
                                            adaptador = new AdaptadorIntegrante(getBaseContext(), alIntegranteA);
                                            listIntegrantes_pii.setAdapter(adaptador);
                                        }


                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {
                                    Toast.makeText(getBaseContext(),"Error al consultar", Toast.LENGTH_SHORT).show();
                                }
                            });

                        }

                    }

                }
                else
                {
                    Toast.makeText(getBaseContext(),"No se ha encontrado informacion", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getBaseContext(),"Error al consultar", Toast.LENGTH_SHORT).show();
            }
        });

        listIntegrantes_pii.setOnItemClickListener((parent, view, position, id) -> {

        });

    }

    public void setCreador(String uid_creador)
    {
        Query query = dr.child("Usuario").child(uid_creador);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists())
                {
                    String nombre = snapshot.child("nombre").getValue().toString();
                    String rol = snapshot.child("rol").getValue().toString();
                    txt_creador_pii.setText(("Creador : " + nombre + " - " + rol));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getBaseContext(),"Error al consultar", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onVerTareas(View view)
    {
        Intent i = new Intent(this, listar_tareas_invitado.class);
        i.putExtra("uid_proyecto", uid_proyectoElegido);
        startActivity(i);
    }
}
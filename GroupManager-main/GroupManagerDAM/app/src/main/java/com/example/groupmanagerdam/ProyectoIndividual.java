package com.example.groupmanagerdam;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.groupmanagerdam.models.integrante;
import com.example.groupmanagerdam.models.integranteModelAdaptador;
import com.example.groupmanagerdam.models.proyecto;
import com.example.groupmanagerdam.models.usuario;
import com.example.groupmanagerdam.utils.AdaptadorIntegrante;
import com.example.groupmanagerdam.utils.AdaptadorProyecto;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Objects;

public class ProyectoIndividual extends BaseMenu {

    public TextView txt_nombre_pi, txt_descripcion_pi, txt_inicio_pi, txt_fin_pi, txt_presupuesto_pi, txt_creador_pi, txt_estado_pi;
    public ListView listIntegrantes;

    String uid_proyectoElegido;
    public ArrayList<integranteModelAdaptador> alIntegranteA;
    public AdaptadorIntegrante adaptador;

    public int contador = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proyecto_individual);

        txt_nombre_pi = findViewById(R.id.txt_nombre_pi);
        txt_descripcion_pi = findViewById(R.id.txt_descripcion_pi);
        txt_inicio_pi = findViewById(R.id.txt_inicio_pi);
        txt_fin_pi = findViewById(R.id.txt_fin_pi);
        txt_presupuesto_pi = findViewById(R.id.txt_presupuesto_pi);
        txt_creador_pi = findViewById(R.id.txt_creador_pi);
        txt_estado_pi = findViewById(R.id.txt_estado_pi);

        listIntegrantes = findViewById(R.id.listIntegrantes);

        iniciarSesion();
        activarPermisos();
        inicializarFirebase();

        uid_proyectoElegido = getIntent().getStringExtra("uid_proyecto");

        Query query = dr.child("Proyecto").orderByChild("uid_proyecto").equalTo(uid_proyectoElegido);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists())
                {
                    for (DataSnapshot data : snapshot.getChildren()) {
                        proyecto pro = data.getValue(proyecto.class);

                        txt_nombre_pi.setText(("Nombre : " + pro.getNombre()));
                        txt_descripcion_pi.setText(("Descripcion : "+pro.getDescripcion()));
                        txt_inicio_pi.setText(("Fecha inicio : " + pro.getFecha_inicio()));
                        txt_fin_pi.setText(("Fecha fin : " + pro.getFecha_fin()));
                        txt_presupuesto_pi.setText(("Presupuesto : " + pro.getPresupuesto()));
                        txt_estado_pi.setText("Estado : " + pro.getEstado());

                        setCreador(pro.getUid_usuario());

                        break;
                    }
                }
                else
                {
                    Toast.makeText(getBaseContext(),"No se ha encontrado informacion", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getBaseContext(),"Error al consultar", Toast.LENGTH_SHORT).show();
            }
        });


        alIntegranteA = new ArrayList<>();

        Query query2 = dr.child("Proyecto").child(uid_proyectoElegido);

        query2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists())
                {
                    listIntegrantes.setAdapter(null);
                    alIntegranteA.clear();

                    if(snapshot.hasChild("listaIntegrante"))
                    {
                        contador = 0;
                        final int maxCount = (int) snapshot.child("listaIntegrante").getChildrenCount();

                        for (DataSnapshot data : snapshot.child("listaIntegrante").getChildren())
                        {
                            integrante integr = data.getValue(integrante.class);

                            String uid_usuario = integr.getUid_usuario();

                            dr.child("Usuario").child(uid_usuario).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                    if(snapshot.exists())
                                    {
                                        integranteModelAdaptador intma = new integranteModelAdaptador();

                                        intma.setCorreo(Objects.requireNonNull(snapshot.child("correo").getValue()).toString());
                                        intma.setNombre(Objects.requireNonNull(snapshot.child("nombre").getValue()).toString());
                                        intma.setConfirmacion(integr.getConfirmacion());
                                        intma.setRol(integr.getRol());
                                        intma.setUid_proyecto(integr.getUid_proyecto());
                                        intma.setUid_usuario(uid_usuario);

                                        alIntegranteA.add(intma);

                                        contador++;

                                        if(contador>=maxCount)
                                        {
                                            adaptador = new AdaptadorIntegrante(getBaseContext(), alIntegranteA);
                                            listIntegrantes.setAdapter(adaptador);
                                        }


                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {
                                    Toast.makeText(getBaseContext(),"Error al consultar", Toast.LENGTH_SHORT).show();
                                }
                            });

                        }

                    }

                }
                else
                {
                    Toast.makeText(getBaseContext(),"No se ha encontrado informacion", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getBaseContext(),"Error al consultar", Toast.LENGTH_SHORT).show();
            }
        });

        listIntegrantes.setOnItemClickListener((parent, view, position, id) -> {

            Intent i = new Intent(this, Editar_Participante.class);
            i.putExtra("uid_proyecto", uid_proyectoElegido);
            i.putExtra("uid_usuario", alIntegranteA.get(position).getUid_usuario());
            startActivity(i);

        });

    }

    public void onCambio(View view)
    {
        Intent i = new Intent(this, ProyectoIndividual_Editar.class);
        i.putExtra("uid_proyecto", uid_proyectoElegido);
        startActivity(i);
    }

    public void onEliminar(View view)
    {

        AlertDialog dialog = new AlertDialog.Builder(this).setMessage(R.string.txt_dialog_borrar_proyecto)
                .setPositiveButton(R.string.txt_si, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        eliminarProyecto();
                    }
                }).setNegativeButton(R.string.txt_no, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        // Do stuff when user neglects.
                    }
                }).setOnCancelListener(new DialogInterface.OnCancelListener() {

                    @Override
                    public void onCancel(DialogInterface dialog) {
                        dialog.dismiss();
                        // Do stuff when cancelled
                    }
                }).create();
        dialog.show();

    }

    public void eliminarProyecto()
    {
        Query query = dr.child("Proyecto").child(uid_proyectoElegido);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists())
                {
                    dr.child("Proyecto").child(uid_proyectoElegido).removeValue();
                    Toast.makeText(getBaseContext(),"Se ha borrado el proyecto", Toast.LENGTH_SHORT).show();

                    finish();
                }
                else
                {
                    Toast.makeText(getBaseContext(),"No se ha encontrado informacion", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getBaseContext(),"Error al consultar", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void setCreador(String uid_creador)
    {
        Query query = dr.child("Usuario").child(uid_creador);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists())
                {
                    String nombre = snapshot.child("nombre").getValue().toString();
                    String rol = snapshot.child("rol").getValue().toString();
                    txt_creador_pi.setText(("Creador : " + nombre + " - " + rol));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getBaseContext(),"Error al consultar", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void copiarCodigoInvitacion(View view)
    {

        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("Texto copiado", uid_proyectoElegido);
        clipboard.setPrimaryClip(clip);

        Toast.makeText(getBaseContext(),"Codigo de invitacion de proyecto copiado al portapapeles", Toast.LENGTH_SHORT).show();


    }

    public void onVerTareas(View view)
    {
        Intent i = new Intent(this, listar_tareas.class);
        i.putExtra("uid_proyecto", uid_proyectoElegido);
        startActivity(i);
    }

}